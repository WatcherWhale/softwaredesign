package be.montra.people;

import be.montra.controllers.Controller;
import be.montra.databases.Database;
import be.montra.interfaces.IIdentifiable;

import javax.swing.*;

public class Person extends IIdentifiable
{
    private String name;

    public Person(String name)
    {
        super();
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @Override
    public String toString()
    {
        return this.name;
    }

    @Override
    public IIdentifiable openCreateDialogBox()
    {
        JTextField name = new JTextField();

        final JComponent[] inputs = new JComponent[]
        {
            new JLabel("Name:"),
            name
        };

        if (JOptionPane.OK_OPTION == JOptionPane.showConfirmDialog(null, inputs,
                "Add a person", JOptionPane.OK_CANCEL_OPTION))
        {
            if (name.getText().isEmpty())
                return null;

            return Controller.getInstance().createPerson(name.getText());
        }

        return null;
    }
}
