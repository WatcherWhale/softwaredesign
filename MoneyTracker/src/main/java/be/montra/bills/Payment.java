package be.montra.bills;

import be.montra.databases.Database;

import java.util.Objects;
import java.util.UUID;

class Payment
{
    private final UUID creditor;
    private final UUID debtor;
    private final double amount;

    public Payment(UUID creditor, UUID debtor, double amount)
    {
        this.creditor = creditor;
        this.debtor = debtor;
        this.amount = amount;
    }

    public UUID getCreditor()
    {
        return creditor;
    }

    public UUID getDebtor()
    {
        return debtor;
    }

    public double getAmount()
    {
        return amount;
    }

    @Override
    public String toString()
    {
        String creditor = Database.getPeopleDatabase().getEntry(this.creditor).toString();
        String debtor = Database.getPeopleDatabase().getEntry(this.debtor).toString();
        String amount = String.format ("%.2f", this.amount);
        return debtor + "→" + amount + "→" + creditor;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Payment payment = (Payment) o;
        return Double.compare(payment.amount, amount) == 0 && Objects.equals(creditor, payment.creditor) && Objects.equals(debtor, payment.debtor);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(creditor, debtor, amount);
    }
}
