package be.montra.bills;

import be.montra.databases.Database;

import java.util.*;
import java.util.function.Consumer;

public class PayGraph implements Iterable<Payment>
{
    private final Bill _bill;
    private final ArrayList<Payment> _payments;

    PayGraph(Bill bill)
    {
        this._bill = bill;
        this._payments = new ArrayList<>();

        this.calculate();
    }

    private void calculate()
    {
        LinkedList<Map.Entry<UUID, Double>> debtors = new LinkedList<>();
        LinkedList<Map.Entry<UUID, Double>> creditors = new LinkedList<>();

        for(UUID id : this._bill.getKeySet())
        {
            Map.Entry<UUID, Double> entry = new AbstractMap.SimpleEntry<>(id, Math.abs(this._bill.getLeftToPaid(id)));

            if(this._bill.getLeftToPaid(id) < 0)
            {
                creditors.add(entry);
            }
            else
            {
                debtors.add(entry);
            }
        }

        creditors.sort(Map.Entry.<UUID, Double>comparingByValue().reversed());

        for(Map.Entry<UUID, Double> creditor : creditors)
        {
            debtors.sort(Map.Entry.<UUID, Double>comparingByValue().reversed());

            double left = creditor.getValue();
            for (int i = 0; i < debtors.size() && left > 0; i++)
            {
                Map.Entry<UUID, Double> debtor = debtors.get(i);

                double paid = Math.min(left, debtor.getValue());
                left -= paid;

                if(paid != 0)
                {
                    this._payments.add(new Payment(creditor.getKey(), debtor.getKey(), paid));

                    debtor.setValue(debtor.getValue() - paid);
                }
                debtors.set(i, debtor);
            }
        }
    }

    public int length()
    {
        return this._payments.size();
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();

        sb.append("<html>");

        UUID lastCreditor = null;
        for (Payment payment : this._payments)
        {
            if(lastCreditor != payment.getCreditor())
            {
                lastCreditor = payment.getCreditor();
                sb.append(payment.toString());
            }
            else
            {
                String payS = payment.toString();
                String creditor = Database.getPeopleDatabase().getEntry(lastCreditor).toString();
                payS = payS.substring(0, payS.length() - creditor.length() - 1);
                payS += "↗";

                sb.append(payS).append("<br>");
            }

            sb.append("<br>");
        }

        sb.append("</html>");
        return sb.toString();
    }

    @Override
    public Iterator<Payment> iterator()
    {
        return this._payments.iterator();
    }

    @Override
    public void forEach(Consumer<? super Payment> consumer)
    {
        this._payments.forEach(consumer);
    }

    @Override
    public Spliterator<Payment> spliterator()
    {
        return this._payments.spliterator();
    }
}
