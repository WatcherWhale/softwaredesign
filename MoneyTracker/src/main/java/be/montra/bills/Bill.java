package be.montra.bills;

import be.montra.databases.Database;
import be.montra.people.Person;
import be.montra.tickets.Ticket;

import java.util.HashMap;
import java.util.Set;
import java.util.UUID;

public class Bill
{
    public HashMap<UUID, Double> _leftToPay;

    public Bill()
    {
        this._leftToPay = new HashMap<>();
        this.calculate();
    }

    public void calculate()
    {
        for (Person p : Database.getPeopleDatabase())
        {
            double leftToPay = 0;

            for (Ticket t : Database.getTicketDatabase())
            {
                if (t.getOwner() == p.getUuid())
                    leftToPay -= t.getBill() - t.getShare(p.getUuid());

                leftToPay += t.getLeftToPay(p.getUuid());
            }

            this._leftToPay.put(p.getUuid(), leftToPay);
        }
    }

    public PayGraph createPayGraph()
    {
        return new PayGraph(this);
    }

    public String[][] generateTable()
    {
        String[][] table = new String[this._leftToPay.size()][];

        int index = 0;
        for (UUID id : this._leftToPay.keySet())
        {
            Person p = Database.getPeopleDatabase().getEntry(id);
            table[index] = new String[]{p.getName(), this._leftToPay.get(id).toString()};
            index++;
        }

        return table;
    }

    public int size()
    {
        return this._leftToPay.size();
    }

    public double getLeftToPaid(UUID person)
    {
        return this._leftToPay.get(person);
    }

    public Set<UUID> getKeySet()
    {
        return this._leftToPay.keySet();
    }
}
