package be.montra.tickets.splitTicket;

import be.montra.tickets.Ticket;

import java.util.UUID;

public class SplitTicket extends Ticket
{
    private double _bill;
    public SplitTicket(String name, UUID owner, double bill)
    {
        super(name, owner);
        this._bill = bill;

        this.addDebtor(owner);
    }

    @Override
    public double getBill()
    {
        return this._bill;
    }

    @Override
    public double getShare(UUID person)
    {
        if (this._debtors.containsKey(person))
            return this._bill / this._debtors.size();
        else
            return 0;
    }

    @Override
    public void addDebtor(UUID debtor)
    {
        super.addDebtor(debtor);
        this.recalculateOwnerShare();
    }

    public void setBill(double bill)
    {
        this._bill = bill;
        this.recalculateOwnerShare();
    }

    public void recalculateOwnerShare()
    {
        this._debtors.put(this._owner, this._bill/this._debtors.size());
    }
}
