package be.montra.tickets.splitTicket;

import be.montra.tickets.Ticket;
import be.montra.tickets.TicketFactory;

import java.util.UUID;

public class SplitTicketFactory extends TicketFactory
{
    @Override
    public Ticket createTicket(String name, UUID owner)
    {
        return new SplitTicket(name, owner, 0);
    }

}
