package be.montra.tickets;

import be.montra.people.Person;
import be.montra.tickets.buyoutTicket.BuyoutTicketFactory;
import be.montra.tickets.splitTicket.SplitTicketFactory;

import java.util.UUID;

public abstract class TicketFactory
{
    public abstract Ticket createTicket(String name, UUID owner);

    public Ticket createTicket(String name, Person owner)
    {
        return this.createTicket(name, owner.getUuid());
    }

    public static TicketFactory getSplitTicketFactory()
    {
        return new SplitTicketFactory();
    }

    public static TicketFactory getBuyoutTicketFactory()
    {
        return new BuyoutTicketFactory();
    }
}
