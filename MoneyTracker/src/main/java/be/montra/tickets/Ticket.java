package be.montra.tickets;

import be.montra.databases.Database;
import be.montra.ui.dialogs.TicketDialog;
import be.montra.interfaces.IIdentifiable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public abstract class Ticket extends IIdentifiable
{
    protected String _name;
    protected UUID _owner;
    protected HashMap<UUID, Double> _debtors;

    public Ticket(String name, UUID owner)
    {
        super();
        this._name = name;
        this._owner = owner;
        this._debtors = new HashMap<>();
    }

    public String getName()
    {
        return this._name;
    }

    public UUID getOwner()
    {
        return this._owner;
    }

    public ArrayList<UUID> getDebtors()
    {
        return new ArrayList<>(this._debtors.keySet());
    }

    public void addDebtor(UUID debtor)
    {
        if(!this._debtors.containsKey(debtor))
            this._debtors.put(debtor, 0d);
    }

    public abstract double getBill();

    public double getPaid()
    {
        return this._debtors.values().stream().mapToDouble(Double::doubleValue).sum();
    }

    public double getPaid(UUID person)
    {
        return this._debtors.get(person);
    }

    public Boolean isComplete()
    {
        return this.getPaid() >= this.getBill();
    }

    public abstract double getShare(UUID person);

    public double getLeftToPay(UUID person)
    {
        if(!this._debtors.containsKey(person))
            return 0;

        return this.getShare(person) - this._debtors.get(person);
    }

    public double pay(UUID person, double amount)
    {
        if(!this._debtors.containsKey(person))
            return amount;

        double currentAmount = amount + this._debtors.get(person);

        if(currentAmount > this.getShare(person))
        {
            this._debtors.put(person, this.getShare(person));
            return currentAmount - this.getShare(person);
        }

        this._debtors.put(person, currentAmount);
        return 0d;
    }

    @Override
    public IIdentifiable openCreateDialogBox()
    {
        return TicketDialog.openDialog();
    }

    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();

        builder.append(this._name).append(" paid by ").append(Database.getPeopleDatabase().getEntry(this._owner));
        builder.append(" \n(").append(this.getPaid()).append("/").append(this.getBill()).append(")");

        return builder.toString();
    }
}
