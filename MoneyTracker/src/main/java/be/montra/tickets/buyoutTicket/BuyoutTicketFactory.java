package be.montra.tickets.buyoutTicket;

import be.montra.tickets.Ticket;
import be.montra.tickets.TicketFactory;

import java.util.UUID;

public class BuyoutTicketFactory extends TicketFactory
{
    @Override
    public Ticket createTicket(String name, UUID owner)
    {
        return new BuyoutTicket(name, owner);
    }

}
