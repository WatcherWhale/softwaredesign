package be.montra.tickets.buyoutTicket;

import be.montra.tickets.Ticket;

import java.util.HashMap;
import java.util.UUID;

public class BuyoutTicket extends Ticket
{
    private final HashMap<UUID, Double> _shares;

    public BuyoutTicket(String name, UUID owner)
    {
        super(name, owner);
        _shares = new HashMap<>();
    }

    @Override
    public double getBill()
    {
        return _shares.values().stream().mapToDouble(Double::doubleValue).sum();
    }

    @Override
    public void addDebtor(UUID debtor)
    {
        super.addDebtor(debtor);
        this._shares.put(debtor, 0d);
    }

    public void setShare(UUID person, double share)
    {
        super.addDebtor(person);
        this._shares.put(person, share);

        if(this._owner == person)
            this._debtors.put(this._owner, share);
    }

    @Override
    public double getShare(UUID person)
    {
        if (this._shares.containsKey(person))
            return this._shares.get(person);
        else
            return 0;
    }
}
