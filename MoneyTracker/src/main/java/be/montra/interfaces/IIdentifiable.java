package be.montra.interfaces;

import java.util.UUID;

public abstract class IIdentifiable
{
    protected UUID _uuid;

    public IIdentifiable()
    {
        this._uuid = UUID.randomUUID();
    }

    public UUID getUuid()
    {
        return _uuid;
    }

    public abstract IIdentifiable openCreateDialogBox();
}
