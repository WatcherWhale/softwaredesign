package be.montra.databases;

import be.montra.people.Person;
import be.montra.tickets.splitTicket.SplitTicket;
import be.montra.tickets.Ticket;
import be.montra.ui.components.DatabasePanel;

import java.util.*;
import java.util.function.Consumer;

public class Database<E> extends Observable implements Iterable<E>
{
    private HashMap<UUID, E> _db;
    private String name;
    private final E _def;

    private Database(E def)
    {
        this.name = "Database";
        this._db = new HashMap<>();
        this._def = def;
    }

    private Database(String name, E def)
    {
        this.name = name;
        this._db = new HashMap<>();
        this._def = def;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.notifyObservers();
        this.name = name;
    }

    public E getEntry(UUID id)
    {
        return this._db.getOrDefault(id, null);
    }

    public void addEntry(UUID id, E entry)
    {
        this._db.put(id, entry);

        this.notifyObservers(id);
    }

    public void removeEntry(UUID id)
    {
        this._db.remove(id);

        this.notifyObservers();
    }

    public void clear()
    {
        this._db.clear();
    }

    @Override
    public void notifyObservers()
    {
        this.setChanged();
        super.notifyObservers();
    }

    @Override
    public void notifyObservers(Object o)
    {
        this.setChanged();
        super.notifyObservers(o);
    }

    public DatabasePanel<E> createPanel()
    {
        return new DatabasePanel<>(this);
    }

    public ArrayList<UUID> getIds()
    {
        return new ArrayList<UUID>(this._db.keySet());
    }

    public E getDefault()
    {
        return this._def;
    }

    @Override
    public Iterator<E> iterator()
    {
        return this._db.values().iterator();
    }

    @Override
    public void forEach(Consumer consumer)
    {
        this._db.values().forEach(consumer);
    }

    @Override
    public Spliterator<E> spliterator()
    {
        return this._db.values().spliterator();
    }

    private static Database<Person> _pDB;
    private static Database<Ticket> _tDB;

    public static Database<Person> getPeopleDatabase()
    {
        if(_pDB == null)
            _pDB = new Database<>("People", new Person(null));

        return _pDB;
    }

    public static Database<Ticket> getTicketDatabase()
    {
        if(_tDB == null)
            _tDB = new Database<>("Tickets", new SplitTicket(null, null, 0));

        return _tDB;
    }
}
