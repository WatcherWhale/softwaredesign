package be.montra.ui.components;

import be.montra.databases.Database;
import be.montra.controllers.Controller;
import be.montra.interfaces.IIdentifiable;

import javax.swing.*;
import java.awt.*;

public class DatabasePanel<E> extends JPanel
{
    private final Database<E> _db;

    private JLabel _nameLabel;

    private JList<E> _listBox;

    public DatabasePanel(Database<E> db)
    {
        this._db = db;
        this.initialize();
    }

    public void initialize()
    {
        DefaultListModel<E> listModel = new DefaultListModel<>();
        for(E entry : this._db)
            listModel.addElement(entry);

        this._nameLabel = new JLabel(this._db.getName());
        this._nameLabel.setFont(new Font("Sans", Font.PLAIN, 20));

        this._listBox = new JList<>(listModel);
        this._listBox.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        JButton addButton = new JButton("+");
        JButton removeButton = new JButton("-");

        addButton.addActionListener(listener -> {
            if(this._db.getDefault() instanceof IIdentifiable)
            {
                Controller.getInstance().createIdentifiableFromDefault((IIdentifiable) this._db.getDefault());
            }
        });

        JPanel buttonPanel = new JPanel();
        buttonPanel.add(addButton);
        buttonPanel.add(removeButton);

        removeButton.addActionListener(listener -> {
            if(this._db.getDefault() instanceof IIdentifiable)
            {
                IIdentifiable id = (IIdentifiable) this._listBox.getSelectedValue();
                Controller.getInstance().removeEntry(id.getUuid(), this._db);
            }
        });


        this.setLayout(new BorderLayout());
        this.add(this._nameLabel, BorderLayout.NORTH);
        this.add(this._listBox, BorderLayout.CENTER);
        this.add(buttonPanel, BorderLayout.SOUTH);
    }

    public void refresh()
    {
        DefaultListModel<E> model = (DefaultListModel<E>) this._listBox.getModel();
        model.clear();

        for(E entry : this._db)
            model.addElement(entry);

        this._listBox.setModel(model);

        this._nameLabel.setText(this._db.getName());

        this.updateUI();
    }
}
