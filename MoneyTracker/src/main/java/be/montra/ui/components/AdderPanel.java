package be.montra.ui.components;

import be.montra.interfaces.IIdentifiable;
import be.montra.ui.Util;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class AdderPanel extends JPanel
{
    private final JList<IIdentifiable> _addedBox;
    private final JList<IIdentifiable> _listBox;

    public AdderPanel(Iterable items)
    {
        this.setLayout(new GridBagLayout());
        GridBagConstraints _constraint = new GridBagConstraints();
        _constraint.gridy = 0;
        _constraint.gridx = 0;
        _constraint.gridheight = 6;

        this._addedBox = new JList<>();
        this._addedBox.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        this.add(this._addedBox, _constraint);

        _constraint.gridx++;
        _constraint.gridy = 3;
        _constraint.gridheight = 1;
        JButton addButton = new JButton("+");

        this.add(addButton, _constraint);
        _constraint.gridy++;

        JButton removeButton = new JButton("-");

        this.add(removeButton, _constraint);

        _constraint.gridheight = 6;
        _constraint.gridy = 0;
        _constraint.gridx++;

        this._listBox = new JList<>();
        DefaultListModel<IIdentifiable> model = new DefaultListModel<>();
        items.forEach(x -> model.addElement((IIdentifiable)x));
        this._listBox.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        this._listBox.setModel(model);
        this.add(this._listBox, _constraint);

        addButton.addActionListener(listener -> {
            int i = this._listBox.getSelectedIndex();

            if(i < 0) return;

            IIdentifiable o = this._listBox.getSelectedValue();

            DefaultListModel<IIdentifiable> addModel = Util.copyListModel(this._addedBox.getModel());
            addModel.addElement(o);

            DefaultListModel<IIdentifiable> listModel = Util.copyListModel(this._listBox.getModel());
            listModel.removeElementAt(i);

            this._addedBox.setModel(addModel);
            this._listBox.setModel(listModel);

            this._listBox.updateUI();
            this._addedBox.updateUI();
        });

        removeButton.addActionListener(listener -> {

            int i = this._addedBox.getSelectedIndex();

            if(i < 0) return;

            IIdentifiable o = this._addedBox.getSelectedValue();

            DefaultListModel<IIdentifiable> addModel = Util.copyListModel(this._addedBox.getModel());
            addModel.removeElementAt(i);

            DefaultListModel<IIdentifiable> listModel = Util.copyListModel(this._listBox.getModel());
            listModel.addElement(o);

            this._addedBox.setModel(addModel);
            this._listBox.setModel(listModel);

            this._listBox.updateUI();
            this._addedBox.updateUI();
        });
    }

    public ArrayList<IIdentifiable> getAdded()
    {
        ArrayList<IIdentifiable> added = new ArrayList<>();

        for (int i = 0; i < this._addedBox.getModel().getSize(); i++)
            added.add(this._addedBox.getModel().getElementAt(i));

        return added;
    }
}
