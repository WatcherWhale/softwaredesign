package be.montra.ui.dialogs.panels;

import be.montra.databases.Database;
import be.montra.people.Person;
import be.montra.tickets.buyoutTicket.BuyoutTicket;
import be.montra.tickets.buyoutTicket.BuyoutTicketFactory;
import be.montra.tickets.Ticket;
import be.montra.interfaces.IIdentifiable;
import be.montra.ui.Util;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.ArrayList;
import java.util.UUID;

public class BuyoutTicketDialogPanel extends TicketDialogPanel
{
    private JTable _shareTable;
    private JTextField _priceBox;
    private JComboBox<IIdentifiable> _peopleBox;

    public BuyoutTicketDialogPanel()
    {
        super();
        this._factory = new BuyoutTicketFactory();
    }

    @Override
    public void initialize()
    {
        this.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;

        this.add(new JLabel("Add person"), c);
        c.gridy = 1;
        c.gridwidth = 2;

        this._peopleBox = new JComboBox<>();
        DefaultComboBoxModel<IIdentifiable> personModel = new DefaultComboBoxModel<>();
        for (Person person : Database.getPeopleDatabase())
            personModel.addElement(person);
        this._peopleBox.setModel(personModel);
        this.add(this._peopleBox, c);

        c.gridx = 2;
        c.gridwidth = 1;

        this._priceBox = new JTextField();
        this._priceBox.setColumns(5);
        this.add(this._priceBox, c);

        c.gridx++;
        this.add(new JLabel("€"), c);

        c.gridx = 0;
        c.gridy++;

        JButton addButton = new JButton("+");
        JButton removeButton = new JButton("-");

        this.add(addButton, c);
        c.gridx++;
        this.add(removeButton, c);

        c.gridx = 0;
        c.gridy++;

        this._shareTable = new JTable(new Object[][] {}, new String[] {"Person", "Share"})
        {
            public boolean editCellAt(int row, int column, java.util.EventObject e)
            {
                return false;
            }
        };
        this._shareTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        this.add(this._shareTable, c);

        addButton.addActionListener(listener ->
        {
            DefaultComboBoxModel<IIdentifiable> comboModel = Util.copyComboModel(this._peopleBox.getModel());
            DefaultTableModel tableModel = Util.copyTableModel(this._shareTable.getModel());

            int i = this._peopleBox.getSelectedIndex();
            IIdentifiable selected = (IIdentifiable) this._peopleBox.getSelectedItem();

            comboModel.removeElementAt(i);
            tableModel.addRow(new Object[]{ selected, Double.parseDouble(this._priceBox.getText()) });

            this._shareTable.setModel(tableModel);
            this._peopleBox.setModel(comboModel);

            this._shareTable.updateUI();
            this._peopleBox.updateUI();
        });

        removeButton.addActionListener(listener ->
        {
            DefaultComboBoxModel<IIdentifiable> comboModel = Util.copyComboModel(this._peopleBox.getModel());
            DefaultTableModel tableModel = Util.copyTableModel(this._shareTable.getModel());

            int i = this._shareTable.getSelectedRow();
            IIdentifiable person = (IIdentifiable) this._shareTable.getValueAt(i, 0);

            tableModel.removeRow(i);
            comboModel.addElement(person);

            this._shareTable.setModel(tableModel);
            this._peopleBox.setModel(comboModel);

            this._shareTable.updateUI();
            this._peopleBox.updateUI();
        });
    }

    @Override
    public ArrayList<UUID> getDebtors()
    {
        ArrayList<UUID> debtors = new ArrayList<>();

        for (int i = 0; i < this._shareTable.getModel().getRowCount(); i++)
        {
            Person p = (Person)this._shareTable.getModel().getValueAt(i,0);
            debtors.add(p.getUuid());
        }

        return debtors;
    }

    @Override
    public void updateTicket(Ticket t)
    {
        for (int i = 0; i < this._shareTable.getModel().getRowCount(); i++)
        {
            Person p = (Person)this._shareTable.getModel().getValueAt(i,0);
            double d = (double)this._shareTable.getModel().getValueAt(i,1);

            ((BuyoutTicket)t).setShare(p.getUuid(), d);
        }
    }
}
