package be.montra.ui.dialogs.panels;

import be.montra.tickets.Ticket;
import be.montra.tickets.TicketFactory;

import javax.swing.*;
import java.util.ArrayList;
import java.util.UUID;

public abstract class TicketDialogPanel extends JPanel
{
    protected TicketFactory _factory;

    public abstract void initialize();
    
    public TicketFactory getFactory()
    {
        return this._factory;
    }

    public abstract ArrayList<UUID> getDebtors();

    public abstract void updateTicket(Ticket t);
}
