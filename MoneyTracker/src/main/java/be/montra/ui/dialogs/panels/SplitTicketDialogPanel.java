package be.montra.ui.dialogs.panels;

import be.montra.databases.Database;
import be.montra.tickets.splitTicket.SplitTicket;
import be.montra.tickets.splitTicket.SplitTicketFactory;
import be.montra.tickets.Ticket;
import be.montra.interfaces.IIdentifiable;
import be.montra.ui.components.AdderPanel;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.UUID;
import java.util.stream.Collectors;

public class SplitTicketDialogPanel extends TicketDialogPanel
{
    private JTextField _priceBox;
    private AdderPanel _adderPanel;

    public SplitTicketDialogPanel()
    {
        super();
        this._factory = new SplitTicketFactory();
    }

    @Override
    public void initialize()
    {
        _priceBox = new JTextField(10);

        this.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;

        this.add(new JLabel("Price:"), c);
        c.gridy++;
        this.add(this._priceBox, c);

        c.gridy++;
        this.add(new JLabel("Debtors:"), c);
        c.gridy++;
        this._adderPanel = new AdderPanel(Database.getPeopleDatabase());
        this.add(this._adderPanel, c);
    }

    @Override
    public ArrayList<UUID> getDebtors()
    {
        return (ArrayList<UUID>) this._adderPanel.getAdded()
                .stream().map(IIdentifiable::getUuid).collect(Collectors.toList());
    }

    @Override
    public void updateTicket(Ticket t)
    {
        try
        {
            ((SplitTicket) t).setBill(Double.parseDouble(this._priceBox.getText()));
        }
        catch (Exception ignored)
        {
            if (JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(null,
                    "You inputted an invalid price.\nDo you want to remove your ticket?",
                    "Remove ticket", JOptionPane.YES_NO_OPTION))
            {
                Database.getTicketDatabase().removeEntry(t.getUuid());
            }
        }
    }
}
