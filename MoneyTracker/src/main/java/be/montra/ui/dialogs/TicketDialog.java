package be.montra.ui.dialogs;

import be.montra.databases.Database;
import be.montra.people.Person;
import be.montra.tickets.Ticket;
import be.montra.controllers.Controller;
import be.montra.ui.dialogs.panels.BuyoutTicketDialogPanel;
import be.montra.ui.dialogs.panels.SplitTicketDialogPanel;
import be.montra.ui.dialogs.panels.TicketDialogPanel;

import javax.swing.*;
import java.awt.*;

enum TicketType {SplitTicket, BuyoutTicket}

public class TicketDialog extends JPanel
{
    JComboBox<TicketType> _typeBox;
    JTextField _nameBox;
    JComboBox<Person> _ownerBox;

    TicketDialogPanel _panel;

    GridBagConstraints _constraint;

    private TicketDialog()
    {
        super();
        this.initialize();
    }

    private void initialize()
    {
        DefaultComboBoxModel<TicketType> typeModel = new DefaultComboBoxModel<>();
        typeModel.addElement(TicketType.SplitTicket);
        typeModel.addElement(TicketType.BuyoutTicket);

        this._typeBox = new JComboBox<>();
        this._typeBox.setModel(typeModel);
        this._typeBox.addActionListener(listener -> this.changePanel((TicketType)this._typeBox.getSelectedItem()));

        DefaultComboBoxModel < Person > personModel = new DefaultComboBoxModel<>();
        Database.getPeopleDatabase().forEach(p -> personModel.addElement((Person)p));
        this._ownerBox = new JComboBox<>();
        this._ownerBox.setModel(personModel);

        this._nameBox = new JTextField(30);

        this._panel = new SplitTicketDialogPanel();
        this._panel.initialize();

        this.setLayout(new GridBagLayout());

        this._constraint = new GridBagConstraints();
        this._constraint.gridx = 0;
        this._constraint.gridy = 0;

        this.add(new JLabel("Ticket name:"), this._constraint);
        this._constraint.gridy = 1;
        this.add(this._nameBox, this._constraint);
        this._constraint.gridy = 2;
        this.add(new JLabel("Owner:"), this._constraint);
        this._constraint.gridy = 3;
        this.add(this._ownerBox, this._constraint);
        this._constraint.gridy = 4;
        this.add(new JLabel("Ticket Type:"), this._constraint);
        this._constraint.gridy = 5;
        this.add(this._typeBox, this._constraint);
        this._constraint.gridy = 6;
        this.add(this._panel, this._constraint);
    }

    public void changePanel(TicketType type)
    {
        if(this._panel != null)
            this.remove(this._panel);

        if (type == TicketType.SplitTicket)
        {
            this._panel = new SplitTicketDialogPanel();
        }
        else if (type == TicketType.BuyoutTicket)
        {
            this._panel = new BuyoutTicketDialogPanel();
        }

        if (this._panel != null)
        {
            this._panel.initialize();
            this.add(this._panel, _constraint);
            this.updateUI();
        }
    }

    public static Ticket openDialog()
    {
        TicketDialog dialog = new TicketDialog();

        if(JOptionPane.OK_OPTION == JOptionPane.showConfirmDialog(null, dialog, "Create ticket",
                JOptionPane.OK_CANCEL_OPTION))
        {
            if (dialog._ownerBox.getSelectedIndex() < 0 || dialog._nameBox.getText().isEmpty())
                return null;

            Ticket t = Controller.getInstance().createTicket(dialog._panel.getFactory(), dialog._nameBox.getText(),
                    ((Person)dialog._ownerBox.getSelectedItem()).getUuid(), dialog._panel.getDebtors());

            dialog._panel.updateTicket(t);
            return t;
        }

        return null;
    }
}

