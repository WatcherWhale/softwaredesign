package be.montra.ui;

import be.montra.interfaces.IIdentifiable;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

public class Util
{
    public static DefaultListModel<IIdentifiable> copyListModel(ListModel<IIdentifiable> model)
    {
        DefaultListModel<IIdentifiable> newModel = new DefaultListModel<>();

        for (int i = 0; i < model.getSize(); i++)
        {
            newModel.addElement(model.getElementAt(i));
        }

        return newModel;
    }

    public static DefaultComboBoxModel<IIdentifiable> copyComboModel(ComboBoxModel<IIdentifiable> model)
    {
        DefaultComboBoxModel<IIdentifiable> newModel = new DefaultComboBoxModel<>();

        for (int i = 0; i < model.getSize(); i++)
        {
            newModel.addElement(model.getElementAt(i));
        }

        return newModel;
    }

    public static DefaultTableModel copyTableModel(TableModel model)
    {
        DefaultTableModel newModel = new DefaultTableModel();
        newModel.setColumnCount(model.getColumnCount());

        for (int r = 0; r < model.getRowCount(); r++)
        {
            Object[] objects = new Object[model.getColumnCount()];
            for (int c = 0; c < model.getColumnCount(); c++)
            {
                objects[c] = model.getValueAt(r, c);
            }

            newModel.addRow(objects);
        }

        return newModel;
    }
}
