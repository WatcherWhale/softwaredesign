package be.montra.ui.windows;

import be.montra.bills.Bill;

import javax.swing.*;
import java.awt.*;

public class BillWindow extends JFrame
{
    Bill _bill;

    public BillWindow(Bill bill)
    {
        this._bill = bill;
    }

    public void initialize()
    {
        this.setLayout(new BorderLayout());

        String[][] tableData = this._bill.generateTable();
        String[] tableColumns = new String[] { "Name", "Left to pay" };

        JTable table = new JTable(tableData, tableColumns);
        table.setEnabled(false);

        this.add(table, BorderLayout.CENTER);

        JButton button = new JButton("Create graph");
        button.addActionListener(listener -> {
            GraphWindow win = new GraphWindow(this._bill.createPayGraph());
            win.initialize();
        });

        this.add(button, BorderLayout.SOUTH);

        this.setVisible(true);
    }
}
