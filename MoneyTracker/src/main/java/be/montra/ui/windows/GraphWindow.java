package be.montra.ui.windows;

import be.montra.bills.Bill;
import be.montra.bills.PayGraph;

import javax.swing.*;
import java.awt.*;

public class GraphWindow extends JFrame
{
    PayGraph _graph;

    public GraphWindow(PayGraph graph)
    {
        this._graph = graph;
    }

    public void initialize()
    {
        this.setLayout(new BorderLayout());

        JLabel label = new JLabel(this._graph.toString(), JLabel.CENTER);
        label.setFont(new Font("Sans", Font.PLAIN, 25));
        this.add(label, BorderLayout.CENTER);

        this.setVisible(true);
    }
}
