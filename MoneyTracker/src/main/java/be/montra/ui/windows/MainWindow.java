package be.montra.ui.windows;

import be.montra.bills.Bill;
import be.montra.controllers.Controller;
import be.montra.databases.Database;
import be.montra.people.Person;
import be.montra.tickets.Ticket;
import be.montra.ui.components.DatabasePanel;

import javax.swing.*;
import java.awt.*;
import java.util.Observable;
import java.util.Observer;

public class MainWindow extends JFrame implements Observer
{
    private static final int WIDTH = 600;
    private static final int HEIGHT = 400;

    private DatabasePanel<Person> _personPanel;
    private DatabasePanel<Ticket> _ticketPanel;

    public MainWindow()
    {
        super("Montra");

        Database.getPeopleDatabase().addObserver(this);
        Database.getTicketDatabase().addObserver(this);
    }

    public void initialize()
    {
        this.setSize(WIDTH, HEIGHT);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.setLayout(new BorderLayout());

        JLabel label = new JLabel();
        label.setText("Montra");
        label.setFont(new Font("Sans", Font.PLAIN, 50));
        this.add(label, BorderLayout.NORTH);

        this._personPanel = Database.getPeopleDatabase().createPanel();
        this._ticketPanel = Database.getTicketDatabase().createPanel();

        JSplitPane panelHolder = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, this._personPanel, this._ticketPanel);
        panelHolder.setDividerLocation(WIDTH/2);
        this.add(panelHolder, BorderLayout.CENTER);

        JButton calculateButton = new JButton("Calculate Bill");
        calculateButton.setFont(new Font("Sans", Font.PLAIN, 30));
        calculateButton.addActionListener(listener ->
        {
            BillWindow bw = new BillWindow(Controller.getInstance().createBill());
            bw.initialize();
        });

        this.add(calculateButton, BorderLayout.SOUTH);

        this.setVisible(true);
    }

    @Override
    public void update(Observable observable, Object o)
    {
        this._personPanel.refresh();
        this._ticketPanel.refresh();
    }
}
