package be.montra.controllers;

import be.montra.bills.Bill;
import be.montra.bills.PayGraph;
import be.montra.databases.Database;
import be.montra.people.Person;
import be.montra.tickets.Ticket;
import be.montra.tickets.TicketFactory;
import be.montra.interfaces.IIdentifiable;

import java.util.ArrayList;
import java.util.UUID;

public class Controller
{
    public void createIdentifiableFromDefault(IIdentifiable defaultIdentifiable)
    {
        defaultIdentifiable.openCreateDialogBox();
    }

    public void removeEntry(UUID id, Database db)
    {
        db.removeEntry(id);
    }

    public Person createPerson(String name)
    {
        Person p = new Person(name);
        Database.getPeopleDatabase().addEntry(p.getUuid(), p);
        return p;
    }

    public Ticket createTicket(TicketFactory factory, String name, UUID owner, ArrayList<UUID> debtors)
    {
        Ticket t = factory.createTicket(name, owner);
        debtors.forEach(t::addDebtor);

        Database.getTicketDatabase().addEntry(t.getUuid(), t);

        return t;
    }

    public Bill createBill()
    {
        return new Bill();
    }

    private static Controller _instance = null;
    public static Controller getInstance()
    {
        if(Controller._instance == null)
            Controller._instance = new Controller();

        return Controller._instance;
    }
}
