package be.montra;

import be.montra.databases.Database;
import be.montra.people.Person;
import be.montra.tickets.buyoutTicket.BuyoutTicket;
import be.montra.tickets.splitTicket.SplitTicket;
import be.montra.ui.windows.MainWindow;

import java.util.Arrays;

public class Main
{
    public static void main(String[] args)
    {
        MainWindow mainWindow = new MainWindow();
        mainWindow.initialize();

        if(args.length > 0 && args[0].equals("populate"))
        {
            Person[] persons = new Person[]{
                    new Person("A"),
                    new Person("B"),
                    new Person("C")
            };

            Arrays.stream(persons).forEach(p -> Database.getPeopleDatabase().addEntry(p.getUuid(), p));

            SplitTicket st = new SplitTicket("SplitTicket", persons[0].getUuid(), 300);
            Arrays.stream(persons).map(Person::getUuid).forEach(st::addDebtor);
            Database.getTicketDatabase().addEntry(st.getUuid(), st);

            SplitTicket st2 = new SplitTicket("Only Owner", persons[1].getUuid(), 100);
            Database.getTicketDatabase().addEntry(st2.getUuid(), st2);

            BuyoutTicket bt = new BuyoutTicket("Buyout", persons[2].getUuid());
            bt.setShare(persons[0].getUuid(), 100);
            bt.setShare(persons[1].getUuid(), 50);
            bt.setShare(persons[2].getUuid(), 10);
            Database.getTicketDatabase().addEntry(bt.getUuid(), bt);
        }
    }
}
