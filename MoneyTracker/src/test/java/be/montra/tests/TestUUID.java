package be.montra.tests;

import be.montra.people.Person;
import org.junit.Assert;
import org.junit.Test;

public class TestUUID
{
    @Test
    public void t_UUID()
    {
        Person p1 = new Person("test");
        Person p2 = new Person("test");

        Assert.assertNotEquals(p1.getUuid(), p2.getUuid());
    }
}
