package be.montra.bills;

import be.montra.controllers.Controller;
import be.montra.databases.Database;
import be.montra.people.Person;
import be.montra.tickets.buyoutTicket.BuyoutTicket;
import be.montra.tickets.splitTicket.SplitTicket;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.UUID;

public class PayGraphTest
{
    Person[] persons;
    UUID buyoutTicket;

    @Before
    public void initialize()
    {
        Database.getPeopleDatabase().clear();
        Database.getTicketDatabase().clear();

        persons = new Person[]{new Person("A"), new Person("B"), new Person("C")};

        Arrays.stream(persons).forEach(p -> Database.getPeopleDatabase().addEntry(p.getUuid(), p));

        SplitTicket st = new SplitTicket("SplitTicket", persons[0].getUuid(), 300);
        Arrays.stream(persons).map(Person::getUuid).forEach(st::addDebtor);
        Database.getTicketDatabase().addEntry(st.getUuid(), st);

        SplitTicket st2 = new SplitTicket("Only Owner", persons[1].getUuid(), 100);
        Database.getTicketDatabase().addEntry(st2.getUuid(), st2);

        BuyoutTicket bt = new BuyoutTicket("Buyout", persons[2].getUuid());
        bt.setShare(persons[0].getUuid(), 100);
        bt.setShare(persons[1].getUuid(), 50);
        bt.setShare(persons[2].getUuid(), 10);
        Database.getTicketDatabase().addEntry(bt.getUuid(), bt);

        this.buyoutTicket = bt.getUuid();
    }

    @Test
    public void testGraphAllTickets()
    {
        Bill b = Controller.getInstance().createBill();
        PayGraph g = b.createPayGraph();

        Assert.assertEquals(2, g.length());

        Payment[] payments = new Payment[] {
                new Payment(this.persons[0].getUuid(), this.persons[1].getUuid(), 100),
                new Payment(this.persons[2].getUuid(), this.persons[1].getUuid(), 50)
        };

        int i = 0;

        for (Payment p : g)
        {
            Assert.assertEquals(payments[i], p);
            i++;
        }
    }

    @Test
    public void testGraphReduced()
    {
        Database.getTicketDatabase().removeEntry(this.buyoutTicket);

        Bill b = Controller.getInstance().createBill();
        PayGraph g = b.createPayGraph();

        Assert.assertEquals(2, g.length());

        Payment[] payments = new Payment[]{
                new Payment(this.persons[0].getUuid(), this.persons[1].getUuid(), 100),
                new Payment(this.persons[0].getUuid(), this.persons[2].getUuid(), 100)
        };

        int i = 0;

        for (Payment p : g)
        {
            Assert.assertTrue(Arrays.asList(payments).contains(p));
            i++;
        }

    }
}