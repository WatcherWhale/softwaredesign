package be.montra.bills;

import be.montra.databases.Database;
import be.montra.people.Person;
import be.montra.tickets.buyoutTicket.BuyoutTicket;
import be.montra.tickets.splitTicket.SplitTicket;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

public class BillTest
{
    Person[] persons;

    @Before
    public void initialize()
    {
        persons = new Person[]{
                new Person("A"),
                new Person("B"),
                new Person("C")
        };

        Arrays.stream(persons).forEach(p -> Database.getPeopleDatabase().addEntry(p.getUuid(), p));

        SplitTicket st = new SplitTicket("SplitTicket", persons[0].getUuid(), 300);
        Arrays.stream(persons).map(Person::getUuid).forEach(st::addDebtor);
        Database.getTicketDatabase().addEntry(st.getUuid(), st);

        SplitTicket st2 = new SplitTicket("Only Owner", persons[1].getUuid(), 100);
        Database.getTicketDatabase().addEntry(st2.getUuid(), st2);

        BuyoutTicket bt = new BuyoutTicket("Buyout", persons[2].getUuid());
        bt.setShare(persons[0].getUuid(), 100);
        bt.setShare(persons[1].getUuid(), 50);
        bt.setShare(persons[2].getUuid(), 10);
        Database.getTicketDatabase().addEntry(bt.getUuid(), bt);
    }

    @Test
    public void testCalculation()
    {
        Bill b = new Bill();
        b.calculate();

        Assert.assertEquals(-100, b.getLeftToPaid(persons[0].getUuid()), 0.0001);
        Assert.assertEquals(150, b.getLeftToPaid(persons[1].getUuid()), 0.0001);
        Assert.assertEquals(-50, b.getLeftToPaid(persons[2].getUuid()), 0.0001);
    }
}