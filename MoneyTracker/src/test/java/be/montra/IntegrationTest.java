package be.montra;

import be.montra.bills.Bill;
import be.montra.bills.PayGraph;
import be.montra.controllers.Controller;
import be.montra.people.Person;
import be.montra.tickets.Ticket;
import be.montra.tickets.buyoutTicket.BuyoutTicket;
import be.montra.tickets.buyoutTicket.BuyoutTicketFactory;
import be.montra.tickets.splitTicket.SplitTicket;
import be.montra.tickets.splitTicket.SplitTicketFactory;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;
import java.util.stream.Collectors;

public class IntegrationTest
{
    public Person[] people;

    @Test
    public void integrationTest()
    {
        // Simulate add person button
        Person p1 = Controller.getInstance().createPerson("p1");
        Person p2 = Controller.getInstance().createPerson("p2");
        Person p3 = Controller.getInstance().createPerson("p3");

        this.people = new Person[] { p1, p2, p3 };

        // Creating uuid list
        ArrayList<UUID> list = (ArrayList<UUID>) Arrays.stream(this.people).map(Person::getUuid).collect(Collectors.toList());

        // Simulate creating a ticket
        Ticket st = Controller.getInstance().createTicket(new SplitTicketFactory(), "Strawberries from Cooperation Hoogstraten",
                p1.getUuid(), list);

        // Simulate the (abstract)panel updating the ticket
        updateSplitTicket(st);

        // Simulate creating a ticket
        Ticket bt = Controller.getInstance().createTicket(new BuyoutTicketFactory(), "Ice cream at the Bouwhoeve",
                p3.getUuid(), list);

        // Simulate the (abstract)panel updating the ticket
        updateBuyoutTicket(bt);

        // Open the bill window
        Bill b = Controller.getInstance().createBill();

        // Open graph window
        PayGraph graph = b.createPayGraph();
    }

    public void updateSplitTicket(Ticket t)
    {
        ((SplitTicket)t).setBill(15);
    }

    public void updateBuyoutTicket(Ticket t)
    {
        for (int i = 0; i < this.people.length; i++)
        {
            ((BuyoutTicket) t).setShare(this.people[i].getUuid(), (i+1));
        }
    }
}
