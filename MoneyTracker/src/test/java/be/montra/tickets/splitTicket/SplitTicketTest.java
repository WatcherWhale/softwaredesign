package be.montra.tickets.splitTicket;

import be.montra.databases.Database;
import be.montra.people.Person;
import be.montra.tickets.TicketFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.UUID;

public class SplitTicketTest
{
    private SplitTicket _ticket;
    private UUID[] _people;

    @Before
    public void initialize()
    {
        Person p1 = new Person("p1");
        Person p2 = new Person("p2");
        Person p3 = new Person("p3");

        Database.getPeopleDatabase().addEntry(p1.getUuid(), p1);
        Database.getPeopleDatabase().addEntry(p2.getUuid(), p2);
        Database.getPeopleDatabase().addEntry(p3.getUuid(), p3);

        _people = new UUID[] {p1.getUuid(), p2.getUuid(), p3.getUuid()};

        this._ticket = (SplitTicket) TicketFactory.getSplitTicketFactory().createTicket("Test", p1);
        this._ticket.setBill(1000);

        this._ticket.addDebtor(p2.getUuid());
        this._ticket.pay(p2.getUuid(), 50);

        this._ticket.addDebtor(p3.getUuid());
        this._ticket.pay(p3.getUuid(), 10);
    }

    @Test
    public void testPay()
    {
        // Test if payments get registered
        Assert.assertEquals(60d + 1000.0/3, this._ticket.getPaid(), 0.001);
        Assert.assertEquals(50d, this._ticket.getPaid(this._people[1]), 0.001);

        this._ticket.pay(this._people[1], 20);

        Assert.assertEquals(80d + 1000.0/3, this._ticket.getPaid(), 0.001);
        Assert.assertEquals(70d, this._ticket.getPaid(this._people[1]), 0.001);

        Assert.assertEquals(70d, this._ticket.pay(this._people[1], 1000.0/3), 0.001);
    }

    @Test
    public void testIsComplete()
    {
        Assert.assertFalse(this._ticket.isComplete());

        this._ticket.pay(this._people[1], 500);
        this._ticket.pay(this._people[2], 500);

        Assert.assertTrue(this._ticket.isComplete());
    }

    @Test
    public void testGetLeftToPay()
    {
        Assert.assertEquals(1000.0/3 - 50, this._ticket.getLeftToPay(this._people[1]), 0.001);
        this._ticket.pay(this._people[1], 50d);
        Assert.assertEquals(1000.0/3 - 100, this._ticket.getLeftToPay(this._people[1]), 0.001);
    }

    @Test
    public void testBill()
    {
        Assert.assertEquals(1000d, this._ticket.getBill(), 0.001);
        this._ticket.setBill(200d);
        Assert.assertEquals(200d, this._ticket.getBill(), 0.001);
    }

    @Test
    public void testGetShare()
    {
        Assert.assertEquals(1000.0/3, this._ticket.getShare(this._people[1]), 0.001);
        Assert.assertEquals(1000.0/3, this._ticket.getShare(this._people[2]), 0.001);
        Assert.assertEquals(1000.0/3, this._ticket.getShare(this._people[0]), 0.001);
        Assert.assertEquals(this._ticket.getShare(this._people[1]), this._ticket.getShare(this._people[2]), 0.001);
    }
}