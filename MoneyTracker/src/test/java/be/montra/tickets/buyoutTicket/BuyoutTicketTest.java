package be.montra.tickets.buyoutTicket;


import be.montra.databases.Database;
import be.montra.people.Person;
import be.montra.tickets.TicketFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.UUID;

public class BuyoutTicketTest
{
    private BuyoutTicket _ticket;
    private UUID[] _people;

    @Before
    public void initialize()
    {
        Person p1 = new Person("p1");
        Person p2 = new Person("p2");
        Person p3 = new Person("p3");

        Database.getPeopleDatabase().addEntry(p1.getUuid(), p1);
        Database.getPeopleDatabase().addEntry(p2.getUuid(), p2);
        Database.getPeopleDatabase().addEntry(p3.getUuid(), p3);

        _people = new UUID[] {p1.getUuid(), p2.getUuid(), p3.getUuid()};

        this._ticket = (BuyoutTicket) TicketFactory.getBuyoutTicketFactory().createTicket("Test", p1);

        this._ticket.addDebtor(p2.getUuid());
        this._ticket.setShare(p2.getUuid(), 300);
        this._ticket.pay(p2.getUuid(), 50);

        this._ticket.addDebtor(p3.getUuid());
        this._ticket.setShare(p3.getUuid(), 700);
        this._ticket.pay(p3.getUuid(), 10);
    }

    @Test
    public void testPay()
    {
        Assert.assertEquals(50, this._ticket.pay(this._people[1], 300), 0.001);
        Assert.assertEquals(0, this._ticket.pay(this._people[2], 300), 0.001);
    }

    @Test
    public void testGetBill()
    {
        Assert.assertEquals(1000d, this._ticket.getBill(), 0.001);
        this._ticket.setShare(this._people[1], 350);
        Assert.assertEquals(1050d, this._ticket.getBill(), 0.001);
    }
}