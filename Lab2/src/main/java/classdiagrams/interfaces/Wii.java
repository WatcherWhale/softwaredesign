package classdiagrams.interfaces;

public class Wii implements VolumeDevice
{
    private double volume;

    public Wii()
    {
        volume = 1;
    }

    @Override
    public void volumeUp()
    {
        volume += 0.1;
        if(volume > 1) volume = 1;
    }

    @Override
    public void volumeDown()
    {
        volume -= 0.1;
        if(volume < 0) volume = 0;
    }

    public double getVolume()
    {
        return volume;
    }
}
