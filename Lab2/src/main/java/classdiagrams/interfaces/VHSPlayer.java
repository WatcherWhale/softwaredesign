package classdiagrams.interfaces;

public class VHSPlayer implements VolumeDevice
{
    int volume;

    public VHSPlayer()
    {
        this.volume = 100;
    }

    @Override
    public void volumeUp()
    {
        this.volume += 10;
        this.constrainVolume();
    }

    @Override
    public void volumeDown()
    {
        this.volume -= 10;
        this.constrainVolume();
    }

    private void constrainVolume()
    {
        if(this.volume > 100) this.volume = 100;
        else if(this.volume < 0) this.volume = 0;
    }

    public int getVolume()
    {
        return volume;
    }
}
