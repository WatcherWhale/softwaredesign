package classdiagrams.interfaces;

import java.util.ArrayList;

public class Remote
{
    private ArrayList<VolumeDevice> _devices;

    public Remote()
    {
        this._devices = new ArrayList<>();
    }

    public void addDevice(VolumeDevice device)
    {
        this._devices.add(device);
    }

    public void lowerVolume()
    {
        this._devices.forEach(VolumeDevice::volumeDown);
    }
}
