package classdiagrams.inheritance;

public class Employee
{
    protected double hourlySalary, hoursWorked;

    public Employee(double hourlySalary, double hoursWorked)
    {
        this.hourlySalary = hourlySalary;
        this.hoursWorked = hoursWorked;
    }

    public double calculateDailySalary()
    {
        return hourlySalary * hoursWorked;
    }
}
