package classdiagrams.inheritance;

public class CustomerService extends Employee
{
    protected double bonusPerCustomer, numberOfCustomers;

    public CustomerService(double hourlySalary, double hoursWorked, double bonusPerCustomer, double numberOfCustomers)
    {
        super(hourlySalary, hoursWorked);

        this.bonusPerCustomer = bonusPerCustomer;
        this.numberOfCustomers = numberOfCustomers;
    }

    @Override
    public double calculateDailySalary()
    {
        return super.calculateDailySalary() + this.bonusPerCustomer * this.numberOfCustomers;
    }
}
