package classdiagrams.abstractions;

public class Circle extends Shape
{
    public Circle(double size, String name)
    {
        super(size, name);
    }

    @Override
    public double calcCircumference()
    {
        return 2 * Math.PI * this.size;
    }

    @Override
    public double calcArea()
    {
        return Math.PI * Math.pow(this.size, 2);
    }
}
