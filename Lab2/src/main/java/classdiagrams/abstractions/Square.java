package classdiagrams.abstractions;

public class Square extends Shape
{
    public Square(double size, String name)
    {
        super(size, name);
    }

    @Override
    public double calcCircumference()
    {
        return this.size * 4;
    }

    @Override
    public double calcArea()
    {
        return Math.pow(this.size, 2);
    }
}
