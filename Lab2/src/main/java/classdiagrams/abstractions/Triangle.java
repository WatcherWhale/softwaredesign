package classdiagrams.abstractions;

public class Triangle extends Shape
{
    public Triangle(double size, String name)
    {
        super(size, name);
    }

    @Override
    public double calcCircumference()
    {
        return this.size * 3;
    }

    @Override
    public double calcArea()
    {
        return 1/2d * this.size * Math.sqrt( Math.pow(this.size, 2) - Math.pow(this.size/2,2) );
    }
}
