package classdiagrams.abstractions;

public abstract class Shape
{
    protected double size;
    private String name;

    public Shape(double size, String name)
    {
        this.name = name;
        this.size = size;
    }

    public String getName()
    {
        return name;
    }

    public abstract double calcCircumference();
    public abstract double calcArea();
}
