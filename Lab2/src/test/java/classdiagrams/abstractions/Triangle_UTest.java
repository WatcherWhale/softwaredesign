package classdiagrams.abstractions;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Triangle_UTest
{
    public Triangle_UTest()
    {
        // Intentionally left blank
    }

    @Before
    public void initialize()
    {

    }

    @Test
    public void t_calcCircumference()
    {
        Triangle triangleUnderTest = new Triangle(3, "triangleUnderTest");
        Assert.assertEquals("Testing circumference - input=3 - should be 9", 9, triangleUnderTest.calcCircumference(), 0.001);

        triangleUnderTest = new Triangle(4, "triangleUnderTest");
        Assert.assertEquals("Testing circumference - input=4 - should be 12", 12, triangleUnderTest.calcCircumference(), 0.001);

    }

    @Test
    public void t_calcArea()
    {
        Triangle triangleUnderTest = new Triangle(3, "triangleUnderTest");
        Assert.assertEquals("Testing area - input=3 - should be 3.9", 3.89711431703, triangleUnderTest.calcArea(), 0.001);

        triangleUnderTest = new Triangle(4, "triangleUnderTest");
        Assert.assertEquals("Testing area - input=4 - should be 6.9", 6.9282032302755, triangleUnderTest.calcArea(), 0.001);
    }

    @Test
    public void t_getName()
    {
        Triangle triangleUnderTest = new Triangle(3, "triangle123");
        Assert.assertEquals("Testing name - input=triangle123, should be triangle123", "triangle123", triangleUnderTest.getName());
    }
}
