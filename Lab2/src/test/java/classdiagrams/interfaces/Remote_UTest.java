package classdiagrams.interfaces;

import org.junit.Test;
import org.junit.Assert;

public class Remote_UTest
{
    @Test
    public void t_TestRemote()
    {
        Remote universalRemote = new Remote();

        Wii wii = new Wii();
        VHSPlayer vhs = new VHSPlayer();

        universalRemote.addDevice(wii);
        universalRemote.addDevice(vhs);

        universalRemote.lowerVolume();

        Assert.assertEquals(0.9, wii.getVolume(), 0.001);
        Assert.assertEquals(90, vhs.getVolume());

        for(int i = 0; i < 10; i++)
            universalRemote.lowerVolume();

        Assert.assertEquals(0, wii.getVolume(), 0.001);
        Assert.assertEquals(0, vhs.getVolume());
    }
}
