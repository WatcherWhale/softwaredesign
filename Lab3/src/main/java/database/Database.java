package database;

import employee.Employee;
import register_entry.RegisterEntry;

import java.util.Observable;

public abstract class Database extends Observable
{
    private static Database instance;

    protected Database()
    {
        Database.instance = this;
    }

    public void addEntry(Employee e, RegisterEntry re)
    {
        this.setChanged();
        this.notifyObservers(e);
    }

    public abstract RegisterEntry getEntry(Employee e);

    public static Database getInstance() throws InstantiationException
    {
        if(Database.instance == null) throw new InstantiationException("The singleton cannot be created from this abstract class.");

        return Database.instance;
    }
}
