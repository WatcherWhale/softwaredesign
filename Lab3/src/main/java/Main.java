import controller.Controller;
import controller.RegistrationController;
import database.Database;
import database.RegistrationDB;
import employee.CustomerService;
import employee.Employee;
import employee.Manager;
import employee.Programmer;
import factory.EmployeeFactory;
import observers.EntryPrintObserver;
import observers.PrintObserver;
import observers.UpdateObserver;
import register_entry.RegisterEntry;

public class Main
{
    public static void main(String[] args)
    {
        Main main = new Main();
        main.run();
    }

    public Main()
    {

    }

    public void run()
    {
        Database timedb = RegistrationDB.getInstance();
        EmployeeFactory factory = new EmployeeFactory();

        timedb.addObserver(new UpdateObserver());
        timedb.addObserver(new EntryPrintObserver());
        timedb.addObserver(new PrintObserver());

        Controller register= new RegistrationController(timedb);

        Employee e1 = factory.getEmployee("Alice", "Programmer");
        Employee e2 = factory.getEmployee("Bob", "CustomerService");
        Employee e3 = factory.getEmployee("Charlie", "Manager");

        register.checkIn(e1);
        register.checkIn(e2);

        register.checkOut(e1);
        register.checkOut(e2);
        register.checkOut(e3);
    }

}
