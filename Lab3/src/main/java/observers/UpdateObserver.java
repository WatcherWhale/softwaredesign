package observers;

import java.util.Observable;
import java.util.Observer;

public class UpdateObserver implements Observer
{
    @Override
    public void update(Observable observable, Object o)
    {
        System.out.println("Database got updated.");
    }
}
