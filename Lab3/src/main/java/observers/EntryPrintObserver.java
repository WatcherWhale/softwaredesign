package observers;

import database.Database;
import employee.Employee;
import register_entry.RegisterEntry;

import java.util.Observable;
import java.util.Observer;

public class EntryPrintObserver implements Observer
{
    @Override
    public void update(Observable observable, Object o)
    {
        Database db = (Database)observable;
        Employee employee = (Employee) o;
        RegisterEntry entry = db.getEntry(employee);

        System.out.println(entry);

    }
}
