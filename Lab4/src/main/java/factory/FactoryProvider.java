package factory;

public class FactoryProvider
{
    public static TeslaFactory getModelSFactory()
    {
        return new ModelSFactory();
    }

    public static TeslaFactory getModel3Factory()
    {
        return new Model3Factory();
    }

    public static TeslaFactory getModelXFactory()
    {
        return new ModelXFactory();
    }

    public static TeslaFactory getModelYFactory()
    {
        return new ModelYFactory();
    }

}
