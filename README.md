# SoftwareDesign

[![](https://gitlab.com/WatcherWhale/softwaredesign/badges/master/pipeline.svg)](https://gitlab.com/WatcherWhale/softwaredesign/-/pipelines)

These are my solutions for the course 5-SoftwareDesign given at the University of Antwerp.

## Testing

All code is automatically tested in this repo. Meaning that all unit tests are run each push. You can find the results on the top. This will probably be a green checkmark, if not one of the tests has failed or test are busy running.

To find an overview of all tests go [here](https://gitlab.com/WatcherWhale/softwaredesign/-/pipelines).

## License

This project is under the students do not copy license. This is a modified MIT license, meaning it is a open license with the sole exception that students cannot use copied pieces of my code within their projects for this course.
Students can however take inspiration or look at the code as much as they like.
