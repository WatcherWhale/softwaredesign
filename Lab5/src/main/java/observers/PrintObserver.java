package observers;

import database.Database;
import employee.Employee;
import register_entry.RegisterEntry;

import java.util.Observable;
import java.util.Observer;

public class PrintObserver implements Observer
{
    @Override
    public void update(Observable observable, Object o)
    {
        Database db = (Database)observable;
        Employee employee = (Employee) o;
        RegisterEntry entry = db.getEntry(employee);

        this.print(employee, entry);
    }

    private void print(Employee e, RegisterEntry re)
    {
        System.out.println(e.getName() +
                " (" + e.getFunction() + ")" +
                " " + re);
    }
}
