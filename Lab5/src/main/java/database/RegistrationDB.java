package database;

import employee.Employee;
import register_entry.RegisterEntry;
import register_entry.RegisterEntryNull;

import java.util.HashMap;

public class RegistrationDB extends Database
{
    private final HashMap<Employee, RegisterEntry> db;

    private RegistrationDB()
    {
        super();
        this.db = new HashMap<>();
    }

    @Override
    public void addEntry(Employee e, RegisterEntry re)
    {
        this.db.put(e, re);
        super.addEntry(e, re);
    }

    @Override
    public RegisterEntry getEntry(Employee e)
    {
        return this.db.getOrDefault(e, new RegisterEntryNull());
    }

    public static Database getInstance()
    {
        try
        {
            return Database.getInstance();
        }
        catch (InstantiationException e)
        {
            return new RegistrationDB();
        }
    }
}
