package view;

import controller.RegistrationController;
import database.Database;
import database.RegistrationDB;
import employee.Employee;
import view.panels.ListPanel;
import view.panels.RegistrationButtonPanel;

import javax.swing.*;
import java.awt.*;
import java.util.Observable;
import java.util.Observer;

public class ViewFrame extends JFrame implements Observer
{
    RegistrationController _controller;
    ListPanel panel;
    RegistrationButtonPanel buttons;

    public ViewFrame(RegistrationController controller)
    {
        super("Registration");
        this._controller = controller;
    }

    public void initialize()
    {
        this.setSize(500, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        GridBagLayout layout = new GridBagLayout();
        this.setLayout(layout);

        // Pass the controller to the ButtonPanel
        buttons = new RegistrationButtonPanel(this._controller);
        panel = new ListPanel();

        this.add(panel);
        this.add(buttons);
        this.setVisible(true);
    }

    @Override
    public void update(Observable observable, Object o)
    {
        this.panel.addEntry(RegistrationDB.getInstance().getEntry((Employee) o));
    }
}
