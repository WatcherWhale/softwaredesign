package view.panels;

import controller.RegistrationController;
import employee.Employee;
import employee.Programmer;
import factory.EmployeeFactory;
import register_entry.RegisterEntry;

import javax.swing.*;

public class RegistrationButtonPanel extends JPanel {

    private JButton checkIn;
    private JButton checkOut;

    private RegistrationController _controller;
    private Employee employee;

    public RegistrationButtonPanel(RegistrationController controller)
    {
        this._controller = controller;
        JLabel label = new JLabel("Registration buttons");
        this.checkIn = new JButton("Check In");
        this.checkOut = new JButton("Check Out");

        EmployeeFactory ef = new EmployeeFactory();
        this.employee = ef.getEmployee("Test Person", "Programmer");
        System.out.println(this.employee);
        addCheckInButtonActionListener();
        addCheckOutButtonActionListener();

        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        this.add(label);
        this.add(this.checkIn);
        this.add(this.checkOut);
    }

    public void addCheckInButtonActionListener()
    {
        this.checkIn.addActionListener(listener -> this._controller.checkIn(this.employee));
    }

    public void addCheckOutButtonActionListener()
    {
        this.checkOut.addActionListener(listener -> this._controller.checkOut(this.employee));
    }





}
