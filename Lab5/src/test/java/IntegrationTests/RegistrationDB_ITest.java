package IntegrationTests;

import controller.RegistrationController;
import database.RegistrationDB;
import employee.Employee;
import employee.Programmer;
import org.junit.Before;
import org.junit.Test;

public class RegistrationDB_ITest
{
    RegistrationController controller;

    @Before
    public void initialise()
    {
        controller = new RegistrationController(RegistrationDB.getInstance());
    }

    @Test
    public void t_AddEmployees()
    {
        Employee e1 = new Programmer("Ali");
        Employee e2 = new Programmer("Bobette");
        Employee e3 = new Programmer("Jens");

        controller.checkIn(e1);
        controller.checkIn(e2);
        controller.checkIn(e3);

        controller.checkOut(e1);
        controller.checkOut(e3);
        controller.checkOut(e2);
    }
}
